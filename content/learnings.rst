Our learnings from nonogram
###########################

:date: 2023-05-31 14:06
:modified: 2023-06-02 09:31
:tags: nonogram, game, project
:category: game
:slug: nonogram-game
:authors: Anamitra, Snigdha, Diya, Atchaya
:summary: Our learnings from writing the nonogram code

Nonograms is a logic puzzle with simple rules and challenging solutions.

The rules are simple.
You have a grid of squares, which must be either filled in black or marked with X. Beside each row of the grid are listed the lengths of the runs of black squares on that row. Above each column are listed the lengths of the runs of black squares in that column. Your aim is to find all black squares.

The code that we are writing should be such that the computer should be able to solve a 5x5 nonogram that is provided.

We used various strategies for this such as: Marking the obvious cells first. For example, if the hint says 4, the three cells in the middle for sure are filled in black. We started working on the clues rows first, then followed by columns.
After the initial strategy, we checked whether any column has met its required number of marked cells while the rows were being filled. We crossed out cells that will not be filled for sure. We kept doing this until the entire nonogram is filled completely.

 
